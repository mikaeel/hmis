<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherprescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otherprescriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prescription_id');
            $table->string('description')->nullable();
            $table->string('name');
            $table->boolean('paid')->default(false);
            $table->double('cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otherprescriptions');
    }
}
