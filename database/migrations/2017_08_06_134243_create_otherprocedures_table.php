<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherproceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otherprocedures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procedure_category_id');
            $table->integer('diagnosis_id')->unsigned();
            $table->string('name');
            $table->double('amount');
            $table->text('description')->nullable();
            $table->boolean('paid')->default(0);
            $table->string('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otherprocedures');
    }
}
