<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned()->index();
            $table->integer('staff_id')->unsigned()->index()->nullable();
            $table->string('type_of_payment');
            $table->integer('mode_of_payment')->nullable();
            $table->integer('appointment_id')->nullable();
            $table->double('amount');
            $table->integer("diagnosis_id")->nullable();
            $table->integer('prescription_id')->nullable();
            $table->enum('status', array('0', '1'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
