<?php

use Illuminate\Database\Seeder;

class PatientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = App\Role::whereName('patient')->firstOrFail();

        foreach (range(1, 1000000) as $index) {
        	$user = factory('App\User')->create(['username' => 'patient-' . $index]);
        	$role->users()->attach($user);
        }
    }
}
