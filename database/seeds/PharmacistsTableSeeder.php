<?php

use Illuminate\Database\Seeder;

class PharmacistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = App\Role::whereName('pharmacist')->firstOrFail();

        $user1 = factory('App\User')->create(['username' => 'pharmacist-1']);
        $user2 = factory('App\User')->create(['username' => 'pharmacist-2']);
        
    	$role->users()->attach($user1);
    	$role->users()->attach($user2);
    }
}
