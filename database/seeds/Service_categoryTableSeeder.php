<?php

use Illuminate\Database\Seeder;
use App\Service_category;
class Service_categoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Service_category::truncate();
       Service_category::create(['name' => 'LAB-TEST']);
       Service_category::create(['name' => 'DRUG']);
       Service_category::create(['name' => 'SERVICE AND PROCEDURE']);
       Service_category::create(['name' => 'IMAGING-SERVICE']);
    }
}
