<?php

use Illuminate\Database\Seeder;

class PrescriptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Get all patients who has diagnoses
        $patients = App\Role::whereName('patient')->firstOrFail()->users()->has('diagnoses')->get();
        $doctor = App\Role::whereName('doctor')->firstOrFail()->users()->first();


        $patients->each(function($patient) use ($doctor) {
	        	factory('App\Prescription')->create([
	        		'patient_id' => $patient->id,
	        		'doctor_id' => $doctor->id,
	        		'diagnosis_id' => $patient->diagnoses()->first()->id,
	        	]);
        }); 
    }
}
