<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
	protected $table = 'hospitalservices';
	
    protected $fillable = ['name', 'price'];

    public function patient_lab_tests()
    {

        return $this->hasMany(patient_lab_tests::class, 'test_id');
    }
    public function paymentmode()
    {
    	return $this->belongsTo(Paymentmode::class);
    }
}
