<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class drug extends Model
{
    protected $table = 'hospitalservices';

   public function assigndrug()
   {
   	return $this->hasOne(Assigndrug::class,'medicine_id','id');
   }

    public function paymentmode()
    {
    	return $this->belongsTo(Paymentmode::class);
    }
    
}
