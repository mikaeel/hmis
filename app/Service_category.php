<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service_category extends Model
{
    public function hospitalservices()
    {
    	return $this->hasMany(Hospitalservice::class);
    }
}
