<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Otherprocedure extends Model
{
   
   public function diagnosis(){

   	return $this->belongsTo(Diagnosis::class);
   }

   public function paymentmodes()
   {
   	return $this->hasMany(Paymentmode::class);
   }
}

