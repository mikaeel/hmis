<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

  protected $fillable = [
  	'name', 'fee'
  ];
    
  public function doctors()
    {
        return $this->belongsToMany(Doctor::class, 'doctor_departments')->withTimestamps();
    }
}
