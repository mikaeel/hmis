<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    public function patient_lab_tests()
    {
        return $this->hasMany(Patient_lab_tests::class,'diagnosis_id');
    }
  public function imagings()
    {
        return $this->hasMany(Imaging::class,'diagnosis_id');
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }

    public function labTests()
    {
    	return $this->hasMany(patient_lab_tests::class, 'diagnosis_id');
    }
   public function diagnosis_payment(){

     return $this->hasMany(Diagnosis_payment::class);
   }
   public function otherprocedures()
   {
    return $this->hasMany(Otherprocedure::class);
   }
   public function appointment(){
    
    return $this->belongsTo(Appointment::class);
   }
}
