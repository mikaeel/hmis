<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Test;
use App\Patient;
use Illuminate\Http\Request;
use App\Service_category;
use App\Paymentmode;
use App\Appointment;
class LaboratoryTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceId=Service_category::wherename('LAB-TEST')->first();
        $laboratoryTests = Test::whereService_category_id( $serviceId)->latest()->get();
        $paymentmodes = Paymentmode::all();
        return view('laboratory-tests.index', compact('laboratoryTests','paymentmodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient, Diagnosis $diagnosis)
    {
        $paymentmodes = Paymentmode::all();

        return view('laboratory-tests.create',compact('paymentmodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        $serviceId=Service_category::wherename('LAB-TEST')->first(); 

        $labTest = new test();
        $labTest->service_category_id=$serviceId;
        $labTest->name = $request->input('name');
        $labTest->price = $request->input('price');
        $labTest->paymentmode_id = $request->input('paymentmode_id');
        $labTest->save();
        
        flash('Success')->success();

        return redirect('/laboratory-tests');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param test $test
     * @internal param LaboratoryTest|\App\LaboratoryTest $laboratoryTest
     */
    public function show($id)
    {
        $laboratoryTest = Test::findOrFail($id);
         $paymentmodes = Paymentmode::all();
        return view('laboratory-tests.edit', compact('laboratoryTest','paymentmodes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function edit($id)
    {
        $laboratoryTest = Test::findOrFail($id);
         $paymentmodes = Paymentmode::all();
        return view('laboratory-tests.edit', compact('laboratoryTest','paymentmodes'));
    }
    public function showdiagnosis(Patient $patient, Appointment $appointment)
    {
      $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
      return view('patients.diagnoses.laboratory-tests.showdiagnosis',compact('patient','appointment','diagnosis'));
    }

    public function allLabtest(Patient $patient,Appointment $appointment,Diagnosis $diagnosis)
    {
      
      $diagnosis = Diagnosis::find($diagnosis->id);
      if($diagnosis !=null){    
         return view('patients.investigations.index',compact('patient','appointment','diagnosis'));
      }else{
      return redirect('patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnoses/investigations');
      }
    }
    public function zeroLabtest(Patient $patient,Appointment $appointment){

      return redirect('patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnoses/investigations');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
        ]);

        $laboratoryTest = Test::findOrFail($id);
        $laboratoryTest->name = $request->input('name');
        $laboratoryTest->price = $request->input('price');
        $laboratoryTest->paymentmode_id = $request->input('paymentmode_id');
        $laboratoryTest->save();

        flash('Updated successfully.')->success();

        return redirect('laboratory-tests');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function destroy($id)
    {
        $laboratoryTest = Test::findOrFail($id);
        $laboratoryTest->delete();

        flash('Deleted successfully.')->success();

        return back();
    }

    public function upload(Request $request)
    {
        $serviceId=Service_category::wherename('LAB-TEST')->first(); 
        // load the rows
        $collections = \Excel::load($request->file('user_file'), function ($reader) {
        })->get();
        $count = 0;
        $is_sheets=0;
  foreach ($collections as $collection) {
     foreach ($collection as $coll) {
    if (is_object($coll)) {
  $is_sheets=1;
    }else
    {
        $is_sheets=0;
    }
}
}

if($is_sheets==1)
{
    $index = $collections->count();
     for($i=0;$i< 1;$i++)
     {
      foreach ($collections[$i] as $row) {
            $test = new Test();
            $test->service_category_id = $serviceId;
            $test->name = $row->name;
            $test->price = $row->price;
            $test->paymentmode_id = $request->input('paymentmode_id');
            $test->save();
            $count++;
     }
 }
}if($is_sheets==0){
   foreach ($collections as $row) {
            $test = new Test();
            $test->service_category_id = $serviceId;
            $test->name = $row->name;
            $test->price = $row->price;
            $test->paymentmode_id = $request->input('paymentmode_id');;
            $test->save();
            $count++;
        }   
}

        flash($count . ' items uploaded successfully.')->success();

        return redirect('laboratory-tests');
    }
}
