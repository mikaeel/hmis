<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index()
    {
        return view('account.index');
    }

    public function updateAccount(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users,id',
            'gender' => 'required',
            'dob' => 'date',
            'email' => 'email',
        ]);

        $user = Auth::user();
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->username = strtolower($request->input('last_name'));
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->password = bcrypt($request->input('last_name'));
        $user->save();

        flash('Account updated successfully!')->success();

        return back();
    }

    public function password()
    {
        return view('account.password');
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        if (Hash::check($request->input('current_password'), Auth::user()->getAuthPassword())) {

        $user = Auth::user();
        $user->password = bcrypt($request->input('password'));
        $user->save();

        flash('Password changed successfully.')->success();
        }


        return back();
    }
}
