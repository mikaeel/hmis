<?php

namespace App\Http\Controllers\pharmacist;

use App\Assigndrug;
use App\Http\Controllers\Controller;
use App\Patient;
use App\prescription;
use App\Payment;
use App\Prescription_payment;
use DB;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (!auth()->user()->hasRole('pharmacist')) {
            return redirect('login');
        }
        
          $patientIds = Payment::wheretype_of_payment("Prescription cost")->wherestatus(true)->pluck("patient_id");
        $patients=patient::find($patientIds);
        return view('pharmacist.prescriptions.index', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $patient
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Patient $patient)
    {
        $prescriptions = prescription::with('patient')->where('patient_id', $patient->id)->latest()->get();
        return view('pharmacist.prescriptions.show', compact('patient', 'prescriptions'));
    }

    public function showdrugs($patId, $presId)
    {
        
         $assigndrugs = DB::table('assigndrugs')
            ->join('hospitalservices', 'assigndrugs.medicine_id', '=', 'hospitalservices.id')
            ->select('prescription_id', 'medicine_id', 'name', 'name', 'done', 'dose', 'assigndrugs.created_at')
            ->where('prescription_id', $presId)
            ->get();
        return view('pharmacist.prescriptions.showdrugs', compact('assigndrugs'));
    }

    public function done($pat, $presc, $drug)
    {

        $assigndrug = Assigndrug::where('prescription_id', $presc)->where('medicine_id', $drug)->first();
        $assigndrug->done = 1;
        $assigndrug->save();
        flash("Successifully!")->success()->important();
        return back();
    }

    public function detailsdrugs($pat, $presc, $drug)
    {

        $assigndrug = Assigndrug::where('prescription_id', $presc)->where('medicine_id', $drug)->first();
        return view('pharmacist.prescriptions.viewdescription', compact('assigndrug'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
        