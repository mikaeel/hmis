<?php

namespace App\Http\Controllers;

use App\Role;
use App\Appointment;
use App\Patient;
use App\Assigndrug;
use App\Payment;
use App\Imaging;
use App\Prescription_payment;
use DB;
use App\Patient_lab_tests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $roles = Role::withCount('users')->get();
        $patients = Role::with('users')->whereName('patient')->first()->users()->count();
        $receptionists = Role::with('users')->whereName('receptionist')->first()->users()->count();
        $doctors = Role::with('users')->whereName('doctor')->first()->users()->count();
        $pharmacists = Role::with('users')->whereName('pharmacist')->first()->users()->count();
        $imaging_tech = Role::with('users')->whereName('imaging')->first()->users()->count();
        $admins = Role::with('users')->whereName('admin')->first()->users()->count();
        $laboratoryAttendants = Role::with('users')->whereName('laboratory-attendant')->first()->users()->count();

        $appointments = collect([]);
        if(auth()->user()->hasRole('doctor')){
            $appointments = Appointment::where('doctor_id', auth()->id())->where("did_visit_doctor",false)->latest()->get();
        }
        if(auth()->user()->hasRole('laboratory-attendant')){

        $lab_tests = Patient_lab_tests::where('result',null)->get();
        $patientIds = $lab_tests->pluck('patient_id');
        $patients = Patient::with('payments')->has('payments')->whereIn('id',$patientIds)->count();

        }     
          if(auth()->user()->hasRole('imaging')){

        $imagings = Imaging::where('result',null)->get();
        $patientIds = $imagings->pluck('patient_id');
        $patients = Patient::with('payments')->has('payments')->whereIn('id',$patientIds)->count();

        }
        
        if(auth()->user()->hasRole('pharmacist')){
        $patientIds = Payment::wheretype_of_payment("Prescription cost")->wherestatus(true)->pluck("patient_id");
        $pharmacypatients=patient::find($patientIds)->count();

        }
        
       $labtest = Patient_lab_tests::where('result','!=',null)->latest()->get();

        $patientIds = $labtest->pluck('patient_id');

    $patient_from_lab =  $appointments->whereIn('patient_id',$patientIds);

        return view('home', compact('roles', 'patients','patient_from_lab', 'pharmacypatients','receptionists', 'doctors', 'pharmacists', 'imaging_tech','admins', 'laboratoryAttendants', 'appointments','pharmace_patients'));
    }
}
