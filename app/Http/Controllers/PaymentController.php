<?php

namespace App\Http\Controllers;

use DB;
use App\Patient;
use App\Payment;
use App\Paymentmode;
use Carbon\Carbon;
class PaymentController extends Controller
{
    public function index()
    {

        if (request('date')) {
            if (request('date') == 'today') {
                $allPayments = Payment::today()->get();
            }
            if (request('date') == 'yesterday') {
                $allPayments = Payment::yesterday()->get();
            }
            if (request('date') == 'last-7-days') {
                $allPayments = Payment::pastNDays(7)->get();
            }
            if (request('date') == 'last-14-days') {
                $allPayments = Payment::pastNDays(14)->get();
            }
            if (request('date') == 'last-30-days') {
                $allPayments = Payment::pastNDays(30)->get();
            }
        } else {
            $allPayments = Payment::latest()->get();
        }
        $patientIds = $allPayments->pluck('patient_id');
        $patients = Patient::whereIn('id', $patientIds)->get();
        $paymentmodes = Paymentmode::all();
        return view('payments.index', compact('allPayments','patients', 'paymentmodes','date'));

    }
}
