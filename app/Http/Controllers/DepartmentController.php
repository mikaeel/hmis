<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\Department;
class DepartmentController extends Controller
{
   public function index()
    {
        $departments = Department::all();
        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //return view('patients.diagnoses.departments.create', compact('patient', 'diagnosis'));
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'fee' => 'required|numeric'
        ]);
     
        $department = new Department();
        $department->name = $request->input('name');
        $department->fee = $request->input('fee');
        $department->save();

        flash('Success')->success();
        return redirect('departments');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Department $Department
     * @internal param department|\App\department $department
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\department $department
     */
    public function edit($id)
    {
        $department = Department::findOrFail($id);
        return view('departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\department $department
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'fee' => 'required|numeric'
        ]);

        $department = Department::findOrFail($id);
        $department->update($request->only('name', 'fee'));

        flash('Updated successfully.')->success();

        return redirect('departments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\department $department
     */
    public function destroy($id)
    {
        $department = Department::findOrFail($id);
        $department->delete();

        flash('Deleted successfully.')->success();

        return back();
    }

    public function upload(Request $request)
    {
        // load the rows
        $rows = \Excel::load($request->file('user_file'), function ($reader) {

        })->get();

        $count = 0;

        foreach ($rows as $row) {
            $Department = new Department();
            $Department->name = $row->name;
            $Department->fee = $row->fee;
            $Department->save();
            $count++;
        }

        flash($count . ' departments uploaded successfully.')->success();

        return redirect('departments');
    }
}
