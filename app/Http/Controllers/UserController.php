<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('doctor')) {
        $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->get();
        $appointments = collect([]);
        $appointments = auth()->user()->patients()->orderBy('created_at', 'desc')->get();
        }
        $users = User::latest()->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderby('name')->get();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'role_id' => 'required',
        ]);

        $user = new User();
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->username = strtolower($request->input('last_name'));
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->password = bcrypt("123456");
        $user->save();

        $role = Role::find($roleId = $request->only('role_id'))->first();
        $role->users()->attach($user);        

        flash('User is added successifully !')->success();

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(User $user)
    {
        $roles = Role::all();
        return view('users.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(User $user)
    {
        $roles = Role::orderby('name')->get();
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'middle_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'required',
            'dob' => 'date',
        ]);

        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->username = strtolower($request->input('last_name'));
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->password = bcrypt("123456");
        $user->save();

        flash('Success!')->success();

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(User $user)
    {
        $user->delete();

        flash('Success')->success()->important();

        return back();
    }
    
    public function resetpassword($userId){

     $user = User::whereId($userId)->first();
     $user->password=bcrypt("123456");
     $user->save();
     flash('Password changed successifully,now your password is 123456 but you may change it')->success()->important();
     return redirect()->back();

    }
    public function administrators()
    {
        $users = Role::with('users')->whereName('admin')->first()->users;
        return view('users.index', compact('users'));
    }

    public function doctors()
    {
        $users = Role::with('users')->whereName('doctor')->first()->users;
        return view('users.index', compact('users'));
    } 


    public function imaging()
    {
        $users = Role::with('users')->whereName('imaging')->first()->users;
        return view('users.index', compact('users'));
    } 

    public function pharmacists()
    {
        $users = Role::with('users')->whereName('pharmacist')->first()->users;
        return view('users.index', compact('users'));     
    } 

    public function laboratoryAttendants()
    {
        $users = Role::with('users')->whereName('laboratory-attendant')->first()->users;
        return view('users.index', compact('users'));     
    }

    public function receptionists()
    {
        $users = Role::with('users')->whereName('receptionist')->first()->users;
        return view('users.index', compact('users'));
    }

    public function patients()
    {
        $users = Role::with('users')->whereName('patient')->first()->users;
        return view('users.index', compact('users'));   
    } 
}
