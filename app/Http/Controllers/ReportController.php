<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\Doctor;
use App\Role;
use App\Patient;
use App\Payment;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with('appointments')->find($doctorIds);
        $appointmentIds = Appointment::whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereIn('appointment_id',$appointmentIds)->sum('amount');
        return view('reports.index',compact('doctors','totalPayments','totalPatients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     
      $start = $request->input("from");
      $end = $request->input("to");

      if(!empty($start) && !empty($end))
      {
        $from = $start.' 00:00:00';
        $to = $end.' 00:00:00';
        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with(['appointments'=>function($query) use ($from,$to){

            $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);

        }])->whereIn('id',$doctorIds)->get();
        $appointmentIds = Appointment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('appointment_id',$appointmentIds)->where('status',"1")->sum('amount');
        return view('reports.index',compact('doctors','totalPayments','totalPatients','from','to'));

      }else{
        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with('appointments')->find($doctorIds);
        $appointmentIds = Appointment::whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereIn('appointment_id',$appointmentIds)->sum('amount');
        return view('reports.index',compact('doctors','totalPayments','totalPatients'));
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
       
       //return Payment::whereappointment_id(31)->get();
       $doctor = Doctor::find($doctor->id); 
       $appointments = Appointment::wheredoctor_id($doctor->id)->latest()->get();
       $appointmentIds = Appointment::wheredoctor_id($doctor->id)->pluck("id");
       $patientIds = $appointments->pluck("patient_id");
       $patients = Patient::whereIn('id',$patientIds)->with(["appointments"=>function($query)use($doctor){
         $query->wheredoctor_id($doctor->id)->get();
       }])->with(['payments'=>function($query)use($appointmentIds){
        $query->WhereIn('appointment_id',$appointmentIds)->get();
       }])->latest()->get();
        return view('reports.show', compact('doctor','patients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
