<?php

namespace App\Http\Controllers;

use App\drug;
use Illuminate\Http\Request;
use App\Service_category;
use App\Paymentmode;
class DrugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceId=Service_category::wherename('DRUG')->first();
        $drugs = drug::whereService_category_id( $serviceId)->latest()->get();
        $paymentmodes = Paymentmode::all();
        return view('pharmacist.drugs.index', compact('drugs','paymentmodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paymentmodes = Paymentmode::all();
        return view('pharmacist.drugs.create',compact('paymentmodes'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        $serviceId=Service_category::wherename('DRUG')->first(); 

        $drug = new drug();
        $drug->service_category_id=$serviceId;
        $drug->name = $request->input('name');
        $drug->price = $request->input('price');
        $drug->paymentmode_id = $request->input('paymentmode_id');
        $drug->save();
        
        flash('Saved successifully')->success();
        return redirect('medicines');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $drug = Test::findOrFail($id);
         $paymentmodes = Paymentmode::all();
        return view('laboratory-tests.edit', compact('drug','paymentmodes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drug = drug::findOrFail($id);
        $paymentmodes = Paymentmode::all();
        return view('pharmacist.drugs.edit', compact('drug','paymentmodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $drugId)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric'
        ]);
        $serviceId=Service_category::wherename('DRUG')->first();
        $drug = Drug::findOrFail($drugId);
        $drug->service_category_id=$serviceId;
        $drug->name = $request->input('name');
        $drug->price = $request->input('price');
        $drug->paymentmode_id = $request->input('paymentmode_id');
        $drug->save();

        flash('Updated successfully')->success();

        return redirect('medicines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param drug $drug
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request, $id)
    {
        $drug = drug::findOrFail($id);

        $drug->delete();

        flash('Deleted successfully')->success();

        return redirect('medicines');
    }

    public function upload(Request $request)
    {
        $serviceId=Service_category::wherename('DRUG')->first();
        // load the rows
        $collections = \Excel::load($request->file('user_file'), function ($reader) {

        })->get();

        $count = 0;

 
                $is_sheets=0;
  foreach ($collections as $collection) {
     foreach ($collection as $coll) {
    if (is_object($coll)) {
  $is_sheets=1;
    }else
    {
        $is_sheets=0;
    }
}
}

if($is_sheets==1)
{
    $index = $collections->count();
     for($i=0;$i< 1;$i++)
     {
       
           foreach ($collections[$i] as $row) {
            $drug = new drug();
            $drug->service_category_id = $serviceId;
            $drug->name = $row->name;
            $drug->price = $row->price;
            $drug->paymentmode_id = $request->input('paymentmode_id');
            $drug->save();
            $count++;
        }
     }
 }
if($is_sheets==0){
   foreach($collections as $row) {
            $drug = new drug();
            $drug->service_category_id = $serviceId;
            $drug->name = $row->name;
            $drug->price = $row->price;
            $drug->paymentmode_id = $request->input('paymentmode_id');
            $drug->save();
            $count++;
        }   
}

        flash($count . ' items uploaded successfully.')->success()->important();

        return redirect('medicines');
    }
}
