<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use App\Subcategory;
use Illuminate\Http\Request;

class ApiController extends Controller
{
   public function categoryDropDownData()
{

   $cat_id = Input::get('cat_id');


   $subcategories = Subcategory::where('category_id', '=', $cat_id)
                  ->orderBy('subcategory_name', 'asc')
                  ->get();

   return Response::json($subcategories);


}
}
