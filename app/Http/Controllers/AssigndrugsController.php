<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Patient;
use App\Prescription;
use App\drug;
use App\Assigndrug;
use App\Payment;
use App\Appointment;
use App\Service_category;
use Illuminate\Http\Request;

class AssigndrugsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient,Appointment $appointment, Diagnosis $diagnosis,Prescription $prescription)
    {
         $pmode = Payment::where('patient_id', $patient->id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;

        $serviceId=Service_category::wherename('DRUG')->first();

        $drugs = drug::whereservice_category_id($serviceId)->wherepaymentmode_id($pmode)->get();
        return view('patients.diagnoses.prescriptions.assign_drugs', compact('patient', 'diagnosis', 'drugs','appointment','prescription'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$x,Appointment $appointment,$y,$prescId)
    {
        $this->validate($request,[
            'drug'=>'required',
            'dose'=>'required'
            ]);
        $assigndrug = new Assigndrug();
        $assigndrug->prescription_id = $prescId;
        $assigndrug->medicine_id = $request->input('drug');
        $assigndrug->dose = $request->input('dose');
        $assigndrug->description = $request->input('description');
        $assigndrug->save();
        flash('Drug added successifully')->success()->important();
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
