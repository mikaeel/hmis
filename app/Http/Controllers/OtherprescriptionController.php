<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Otherprescription;
use App\Patient;
use App\Prescription;
use App\Appointment;
use Illuminate\Http\Request;

class OtherprescriptionController extends Controller
{
    public function create(Patient $patient, Appointment $appointment,Diagnosis $diagnosis, Prescription $prescription)
    {

        return view('patients.diagnoses.prescriptions.otherprescriptioncost', compact('patient','appointment','diagnosis','prescription'));
    }

    public function store(Request $request, Patient $patient, Appointment $appointment,Diagnosis $diagnosis, Prescription $prescription)
    {

        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required|numeric'
        ]);
        $otherprescription = new Otherprescription();
        $otherprescription->prescription_id = $prescription->id;
        $otherprescription->name = $request->input('name');
        $otherprescription->cost = $request->input('cost');
        if($request->file('attachment')){
        $path = $request->file('attachment')->store('attachments');
        $otherprescription->attachment = $path;
        }
        $otherprescription->save();

        flash('Successfully Saved')->success()->important();

        return back();
    }
}
