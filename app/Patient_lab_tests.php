<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient_lab_tests extends Model
{
    public function diagnosis()
    {
        return $this->belongsTo(Diagnosis::class);
    }

    public function labTest()
    {
        return $this->belongsTo(Test::class, 'lab_testId');
    }

    public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id');
    } 
}
