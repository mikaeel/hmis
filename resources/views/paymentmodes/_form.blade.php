<div class="form-group">
    <label for="name" class="col-sm-2 col-sm-offset-2 control-label">Name</label>
    <div class="col-sm-6">
        <input type="text" name="name" id="name" value="{{ $paymentmode->name or old('name') }}"
               class="form-control">
    </div>
</div>
<div class="form-group">
	<label for="consultationfee" class="col-sm-4 col-sm-offset-0 control-label">Enter consultation Fee</label>
	<div class="col-sm-6">
		<input type="number" name="consultationfee" id="consultationfee" class="form-control" value="{{ $paymentmode->consultation_fee or old('consultationfee')}}" required="required">
	</div>
</div>



