<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'HMS') }}</title>
       <script src="/js/jquery.js"></script>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
     <link href="/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="/js/bootstrap-datepicker.js"></script>

</head>
<body>

<div id="app">

    @include('partials.navbar')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                @include('flash::message')
            </div>
        </div>
    </div>

    <ol class="breadcrumb">
        <li>
            <a href="/">Home</a>
        </li>
        @if(Request::segment(1))
            <li>
                {{ Request::segment(1) }}
            </li>
        @endif

    </ol>

    @yield('content')

</div>


<script src="/js/jquery-1.12.4.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/select2.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/dropzone.js"></script>
<script>
    $(document).ready(function () {
        $('select').select2();
        $('#flash-overlay-modal').modal();
        $('table').DataTable();
    });
</script>
</body>
</html>