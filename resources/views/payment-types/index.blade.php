@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/payment-types/create" class="btn btn-primary">New</a>
                    </div>

                    <div class="panel-body">
                        @if($paymentTypes->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Display Name</th>
                                    <th>Amount</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($paymentTypes as $paymentType)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            {{ $paymentType->display_name }}
                                        </td>
                                        <td>
                                            <a href="#">
                                                {{ $paymentType->amount }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $paymentType->created_at }}
                                        </td>
                                        <td>
                                            {{ $paymentType->updated_at }}
                                        </td>
                                        <td>
                                            <a href="/payment-types/{{ $paymentType->id }}/edit"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal" href='#modal-id'><i
                                                        class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="modal-id">
                                                <div class="modal-dialog">
                                                    <form action="/payment-types/{{ $paymentType->id }}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">
                                                                    Delete {{ $paymentType->name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete permanently {{ $paymentType->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Payment Type
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
