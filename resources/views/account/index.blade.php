@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">Account</div>

                    <div class="panel-body">
                        <div class="row">
                            <h2 class="text-center">Your Account</h2>
                            <div class="col-sm-3">
                                @include('account.sidebar')
                            </div>
                            <div class="col-sm-5">

                                @include('errors.list')

                                <form action="/account" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}

                                    <div class="form-group">
                                        <label for="">First Name</label>
                                        <input type="text" name="first_name" value="{{ Auth::user()->first_name }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Middle Name</label>
                                        <input type="text" name="middle_name"
                                               value="{{ Auth::user()->middle_name }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Last Name</label>
                                        <input type="text" name="last_name" value="{{ Auth::user()->last_name }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Username</label>
                                        <input type="text" name="username" value="{{ Auth::user()->username }}"
                                               class="form-control">
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="gender"
                                                   value="Female" {{ Auth::user()->gender == 'Female' ? 'checked' : '' }}>
                                            Female
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="gender"
                                                   value="Male" {{ Auth::user()->gender == 'Male' ? 'checked' : '' }}>
                                            Male
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Date of Birth</label>
                                        <input type="date" name="dob"
                                               value="{{ isset(Auth::user()->dob) ? Auth::user()->dob->format('Y-m-d') : '' }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Phone</label>
                                        <input type="text" name="phone" value="{{ Auth::user()->phone }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" name="email" value="{{ Auth::user()->email }}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Address</label>
                                        <input type="text" name="address" value="{{ Auth::user()->address }}"
                                               class="form-control">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Update</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
