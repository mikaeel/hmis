@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $doctor->first_name }}
                            {{ $doctor->middle_name }}
                            {{ $doctor->last_name }}
                        </h3>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <tbody>
                            <tr>
                                <th>Full Name</th>
                                <td>
                                    {{ $doctor->first_name }}  {{ $doctor->middle_name }}  {{ $doctor->last_name }}
                                </td>
                            </tr>

                            <tr>
                                <th>Username</th>
                                <td>
                                    {{ $doctor->username }}
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $doctor->email }}</td>
                            </tr>
                            <tr>
                                <th>Department(s)</th>
                                <td>
                                    @if($doctor->departments->count())
                                        <table class="table table-bordered">
                                            @foreach($doctor->departments as $department)
                                                <tr>
                                                    <td>{{ $department->name }}</td>
                                                    @if(auth()->user()->isAdmin())
                                                        <td>
                                                            <form action="/doctors/{{ $doctor->id }}/departments/{{ $department->id }}" method="POST">
                                                                {{ csrf_field() }}
                                                                {{ method_field('PATCH') }}
                                                                <button type="submit" class="btn btn-danger btn-sm">&times; Remove</button>
                                                            </form>
                                                        </td>
                                                        @endif
                                                </tr>
                                            @endforeach
                                        </table>
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                @if(auth()->user()->isAdmin())
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Assign Department</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-10">
                                    <form action="/doctors/{{ $doctor->id }}/departments" method="POST" class="form-horizontal" role="form">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="for departmet" class="col-md-4 control-label">Choose
                                                Department</label>
                                            <div class="col-md-5 ">
                                                <select class="form-control" name="department" id="department">
                                                    <option value="">select department</option>
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                <button type="submit" class="btn btn-primary">Assign</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
            </div>
        </div>
    </div>
@endsection
