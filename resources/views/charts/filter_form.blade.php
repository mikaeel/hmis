<form action="" method="POST" class="form-inline" role="form">

    <div class="form-group">
        <label for="time_range">Tshs <span id="totalAmount"></span> collected </label>
        <select name="time_range" id="time_range" class="form-control">
            <option value="start={{ \Carbon\Carbon::today()->format('Y-m-d') }}&end={{ \Carbon\Carbon::now()->format('Y-m-d') }}">
                Today
            </option>
            <option value="start={{ \Carbon\Carbon::yesterday()->format('Y-m-d') }}&end={{ \Carbon\Carbon::yesterday()->format('Y-m-d') }}">
                Yesterday
            </option>
            <option value="start={{ \Carbon\Carbon::now()->subWeek(1)->format('Y-m-d') }}&end={{ \Carbon\Carbon::now()->format('Y-m-d') }}">
                Last 7
                days
            </option>
            <option value="start={{ \Carbon\Carbon::today()->subDays(14)->format('Y-m-d') }}&end={{ \Carbon\Carbon::today()->format('Y-m-d') }}">
                Last 14 days
            </option>
            <option value="start={{ \Carbon\Carbon::today()->subDays(30)->format('Y-m-d') }}&end={{ \Carbon\Carbon::today()->format('Y-m-d') }}">
                Last 30 days
            </option>
            <option value="">

            </option>
        </select>
    </div>


    <button type="button" id="btnApply" class="btn btn-primary">Apply</button>
</form>