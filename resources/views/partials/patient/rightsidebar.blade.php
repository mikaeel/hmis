<div class="list-group">
    <a href="/patients/{{ $patient->id }}" class="list-group-item"><i class="fa fa-info-circle fa-fw"></i>
        Information</a>
    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses" class="list-group-item"><i class="fa fa-heartbeat"></i> Medical
        History</a>
</div>