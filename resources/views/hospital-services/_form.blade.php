<div class="form-group">
    <label for="name" class="col-sm-3 control-label">Type of service</label>
    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ $service->name or old('name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="amount" class="col-sm-3 control-label">Price</label>
    <div class="col-sm-9">
        <input type="number" name="price" id="price" value="{{ $service->price or old('price') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="paymentmode" class="col-sm-3 control-label">Mode of payment</label>
    <div class="col-sm-9">
    <select name="paymentmode" id="paymentmode" class="form-control" required="required">
        @foreach($paymentmodes as $pmode)
        <option value="{{$pmode->id}}">{{$pmode->name}}</option>
        @endforeach
      </select>
    </div>
</div>


