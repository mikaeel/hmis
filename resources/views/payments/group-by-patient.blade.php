<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>SN</th>
        <th>PATNO</th>
        <th>PATIENT NAME</th> 
        <th>ACTION</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1
    @endphp
    @foreach($patients as $patient)
        <tr>
            <td>{{ $i++ }}</td>
                <td>
                <a href="/patients/{{ $patient->id }}">
                                                @php 
                                                $id= $patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                             </a>
            </td>
            <td><a href="/patients/{{ $patient->id }}">
                            {{ $patient->first_name }}
                            {{ $patient->middle_name }}
                            {{ $patient->last_name }}
                </a>
            </td>

            <td><a href="/patients/{{$patient->id}}/appointments"><i class="fa fa-eye"></i>&nbsp;View Full Details</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>