@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <!-- Today -->
                @component('components.panel', ['title' => 'Today (TZS)', 'stats' => $todayPaymentStats])

                @endcomponent
                <!-- // Today -->

                <!-- Last 7 days -->
                @component('components.panel', ['title' => 'Last 7 days (TZS)', 'stats' => $last7DaysPaymentStats])

                @endcomponent
                <!-- // Last 7 days -->

                <!-- All -->
                @component('components.panel', ['title' => 'All (TZS)', 'stats' => $allPaymentStats])

                @endcomponent
                <!-- // All -->                

            </div>
        </div>
    </div>
@endsection
