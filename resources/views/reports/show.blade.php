@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Appointments</h3>
                    </div>

                    <div class="panel-body">
                        @if($patients->count())
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Patient</th>
                                    <th>Visit</th>
                                    <th>Total payments</th>
                                   
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($patients as $patient)
                              
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="/patients/{{ $patient->id }}">
                                                {{ $patient->first_name }}
                                                {{ $patient->middle_name }}
                                                {{ $patient->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                          {{$patient->appointments->count() }}
                                        </td>
                                        <th> 
                                            {{ number_format($patient->payments->sum("amount")) }}
                                        </th>
                                       
                                    </tr>
                                   
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Appointment
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
