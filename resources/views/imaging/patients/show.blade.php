@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Imaging investigation</div>

                    <div class="panel-body">
                        @if($imagings->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Test</th>
                                    <th>Result</th>
                                    <th>Action</th>
                                    <th>Conducted</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($imagings as $imaging)
                                    <tr>
                                        <td class="text-right">{{$imaging->id}}</td>
                                        <td>
                                            {{ $imaging->name }}
                                        </td>
                                        <td>
                                            @if($imaging->result)
                                                {{ $imaging->result }}
                                            @else
                                                Not Conducted
                                            @endif
                                        </td>
                                        <td>
                                            @if($imaging->is_conducted == false)
                <a href="/patient/{{$patient->id}}/imagingservices/{{ $imaging->id }}/conduct"
                                                   class="btn btn-primary btn-sm">
                                                    Conduct
                                                </a>
                                            @else
                                                <span class="label label-success">Conducted</span>
                                            @endif
                                        </td>
                                        <td>{{ $imaging->updated_at->diffForHumans() }}</td>
                                        <td>{{ $imaging->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Imaging investigation to conduct
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
