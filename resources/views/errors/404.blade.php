@extends('layouts.app')

@section('content')
	<div class="container text-center">
		<h1>Page Not Found</h1>
		<a href="/">Go home</a>
	</div>
@endsection