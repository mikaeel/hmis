@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                       @include('partials.patient.top_nav_bar') 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>PATIENT'S COMPLAINS</strong></h3>
                            </div>
                             @include('errors.list')
                             <div class="panel-body">
                                <form action="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/save" method="POST" role="form">
                                    {{ csrf_field() }}
                                    @include('patients.diagnoses._form')  
                             </div>
                            <div class="panel-footer">
                            <div class="form-group text-center">
                        <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses" class="btn btn-primary">CANCEL</a> 
                                    <button type="submit" class="btn btn-primary">SAVE</button>
                            </div>
                             </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
