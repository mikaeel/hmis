@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10"> 
                 @include('partials.patient.top_nav_bar')
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">PATIENT FILE FOR: <srong>{{ $patient->first_name}} {{$patient->last_name}}</srong>  <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/"class="pull-right fa fa-arrow-left">Back</a></h3>
                            </div>

                            <div class="panel-body">
                                
                                @if($diagnosis->body)
                                     <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#history"><strong>HISTORY OF PRESENTING ILLINESS</strong></button>
                                       <div class="well well-lg collapse" id="history" style="background:#FEFFFC;">
                                    {{ $diagnosis->body }}
                                  </div>
                                @endif
        <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#physical_examination"> <strong>PHYSICAL EXAMINATION</strong></button>
                                <div class="well well-lg collapse" id="physical_examination" style="background:#FEFFFC;">
                                      {{ $diagnosis->summary }}  
                                </div>
                                 <button type="button" class="btn btn-default" data-toggle="collapse" data-target="#provisional_diagnosis">  <strong>PROVISIONAL DIAGNOSIS</strong></button>
                                <div class="well well-lg collapse" id="provisional_diagnosis" style="background:#FEFFFC;">
                                      {{ $diagnosis->provisional_diagnosis }}  
                                </div>
                            </div>
                            <div class="panel-footer">
                                <a class="fa fa-eye" href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/{{ $diagnosis->id}}/prescriptions">View all
                                    Prescriptions</a>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Imaging services</h3>
                            </div>
                            <div class="panel-body">
                               <div class="row">
                                        @if($diagnosis->imagings->count())
                                        <table class="table table-bordered table-hover">
                                            
                                            <tr>
                                                <th>SN</th>
                                                <th>NAME</th>
                                                <th>RESULT</th>
                                                <th>Created</th>
                                                <th>Click</th>
                                            </tr>
                                           
                                            <tbody>
                                            @php $i = 1 @endphp
                                            @foreach($diagnosis->imagings as $imaging)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $imaging->name }}</td>
                                                    <td>{{ $imaging->result }}</td>
                                                    <td>{{ $imaging->created_at->diffForHumans() }}</td>
                                                    <td>
    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/imagingservice/{{$imaging->id}}/attachment">   <i class="fa fa-eye"></i>&nbsp;View details</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">No Imaging services</div>
                                    @endif  
                               </div> 
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Laboratory Tests</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    @if($diagnosis->patient_lab_tests->count())
                                        <table class="table table-bordered table-hover">
                                            
                                            <tr>
                                                <th>SN</th>
                                                <th>TEST NAME</th>
                                                <th>RESULT</th>
                                                <th>CREATED</th>
                                                <th>ClICK</th>
                                            </tr>
                                           
                                            <tbody>
                                            @php $i = 1 @endphp
                                            @foreach($diagnosis->patient_lab_tests as $patient_lab_test)
                                            @if($patient_lab_test->labTest)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $patient_lab_test->labTest->name }}</td>
                                                    <td>{{ $patient_lab_test->result }}</td>
                                                    <td>{{ $patient_lab_test->created_at->diffForHumans() }}</td>
                                                    <td>
                                                      <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests/{{$patient_lab_test->id}}/view">   <i class="fa fa-eye"></i>&nbsp;View details</a>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">No Lab Tests</div>
                                    @endif
                                </div>

                            </div>
                               <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Procedures</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    @if($procedures->count())
                                        <table class="table table-bordered table-hover">
                                            
                                            <tr>
                                                <th>SN</th>
                                                <th>PROCEDURE NAME</th>
                                                <th>RESULT</th>
                                                <th>CREATED</th>
                                                <th>ClICK</th>
                                            </tr>
                                           
                                            <tbody>
                                            @php $i = 1 @endphp
                                            @foreach($procedures as $procedure)
                                            
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $procedure->name }}</td>
                                                    <td>{{ $procedure->description }}</td>
                                                    <td>{{ $procedure->created_at->diffForHumans() }}</td>
                                                    <td>
                                                      <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/procedure/{{$procedure->id}}/view">   <i class="fa fa-eye"></i>&nbsp;View details</a>
                                                    </td>
                                                </tr>
                                              
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">No procedure found yet</div>
                                    @endif
                                </div>

                            </div>
                             @if($appointment->doctor_id==auth()->user()->id)
                                   <div class="panel-footer">
                                        <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/prescriptions/create"
                                   class="btn btn-primary col-sm-offset-5">Prescribe</a>
                                </div>
                                @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </div>
@endsection
