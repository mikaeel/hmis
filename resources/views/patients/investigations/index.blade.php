@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                @include('partials.patient.top_nav_bar')
                       <div class="panel panel-default">
                            <div class="panel-heading">
                <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests"
                                   class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add new Laboratory tests</a>    
                <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/investigations" class="pull-right badge">Click to view previous diagnoses</a>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    @if($diagnosis->patient_lab_tests->count())
                                        <table class="table table-bordered table-hover">
                                            
                                            <tr>
                                                <th>SN</th>
                                                <th>TEST NAME</th>
                                                <th>RESULT</th>
                                                <th>CREATED</th>
                                                <th>ClICK</th>
                                            </tr>
                                           
                                            <tbody>
                                            @php $i = 1 @endphp
                                            @foreach($diagnosis->patient_lab_tests as $patient_lab_test)
                                            @if($patient_lab_test->labTest)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $patient_lab_test->labTest->name }}</td>
                                                    <td>{{ $patient_lab_test->result }}</td>
                                                    <td>{{ $patient_lab_test->created_at->diffForHumans() }}</td>
                                                    <td>
                                                      <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests/{{$patient_lab_test->id}}/view">   <i class="fa fa-eye"></i>&nbsp;View details</a>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">No Lab Tests</div>
                                    @endif
                                </div>

                            </div>
                             @if($appointment->doctor_id==auth()->user()->id)
                                   <div class="panel-footer">
                                </div>
                                @endif
                        </div> 
                </div>
            </div>
        </div>
    </div>
@endsection
