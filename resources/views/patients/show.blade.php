@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
             @if (Auth::user()->hasRole('doctor'))
            <div class="col-md-10">
                    @include('partials.patient.top_nav_bar') 
             @else
             <div class="col-md-8">
             @endif
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    {{ $patient->first_name }}
                                    {{ $patient->middle_name }}
                                    {{ $patient->last_name }}
                                </h3>
                            </div>

                            <div class="panel-body">

                                <table class="table table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                        <th>Patient ID.</th>
                                        <td>
                                             @php 
                                                $id= $patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>First name</th>
                                        <td>{{ $patient->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Middle name</th>
                                        <td>{{ $patient->middle_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last name</th>
                                        <td>{{ $patient->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Gender</th>
                                        <td>{{ $patient->gender }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth</th>
                                        <td>{{ $patient->dob }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $patient->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $patient->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>{{ $patient->address }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if (Auth::user()->hasRole('receptionist'))
                                <br><br><a href="/patients/{{ $patient->id }}/appointments/create"
                                           class="btn btn-primary">Make
                                    Appointment</a>
                            @endif
                        </div>
                @if (Auth::user()->hasRole('receptionist'))
                  </div>
                 <div class="col-sm-2">
                    @include('partials/patient/sidebar')
                @endif
            </div>
    </div>
@endsection
