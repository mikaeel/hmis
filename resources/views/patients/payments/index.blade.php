@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
    <div class="row">
                    <div class="col-sm-12">
                        <div class="well">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                            </h2>
                            <p>
                                DOB: {{ $patient->dob->toDateString() }} (Born: {{ $patient->dob->diffForHumans() }})
                            </p>
                            <p>
                                <i class="fa fa-phone"></i> {{ $patient->phone }}
                                <i class="fa fa-envelope"></i> {{ $patient->email }}
                                <i class="fa fa-map-marker"></i> {{ $patient->address }}
                            </p>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-10">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Patient payments <a href="/patients/{{ $patient->id }}/appointments"class="pull-right fa fa-arrow-left">Back</a></h3>
                        </div>

                        <div class="panel-body">
                            @if($patient->payments->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Payment type</th>
                                        <th class="text-right">Total Amount (TZS)</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th>Mode</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1
                                    @endphp
                                    @foreach($patient->payments as $payment)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>{{$payment->type_of_payment}}
                                            <td class="text-right">{{ number_format($payment->amount) }}</td>
                                            <td>
                                                @if($payment->status == 1)
                                                    <span class="label label-success">Paid</span>
                                                @else
                                                    <a class="btn btn-warning btn-sm" data-toggle="modal" href='#pay-{{ $payment->id }}'>Pay</a>
                                                    <div class="modal fade" id="pay-{{ $payment->id }}">
                                                        <div class="modal-dialog">
                                                            <form action="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/payments/outstands/{{$payment->id}}" method="post">
                                                            {{ csrf_field()}}

                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title">Confirm Payment</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    Confirm payment for 
                                                                    <strong>
                                                                        {{ $patient->first_name }}
                                                                        {{ $patient->middle_name }}
                                                                        {{ $patient->last_name }}    
                                                                    </strong>

                                                                    Amount (TZS) <strong>{{ number_format($payment->amount) }}</strong>/=
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                    <button type="submit" class="btn btn-primary">Confirm</button>
                                                                </div>
                                                            </div>

                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{ $payment->created_at->toFormattedDateString() }}</td>
                                            <td>{{$paymentmode->name }}</td>
                                            <td>
                                                <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/payment/{{$payment->id}}/details"><i class="fa fa-eye"></i>&nbsp;Click
                                                    to
                                                    View</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <div class="alert alert-info">
                                        This patient has not make any payments yet.
                                    </div>
                                    @endif
                                </div>
                                </div>

                            </div>
                            <div class="col-sm-2">
                                @include('partials/patient/sidebar')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection
