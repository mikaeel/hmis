@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            @include('partials.sidebar')
        </div>
        <div class="col-md-10">
     <div class="row">
                    <div class="col-sm-12">
                        <div class="well">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                            </h2>
                            <p>
                                DOB: {{ $patient->dob->toDateString() }} (Born: {{ $patient->dob->diffForHumans() }})
                            </p>
                            <p>
                                <i class="fa fa-phone"></i> {{ $patient->phone }}
                                <i class="fa fa-envelope"></i> {{ $patient->email }}
                                <i class="fa fa-map-marker"></i> {{ $patient->address }}
                            </p>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-sm-10">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                         <a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/payments" class="fa fa-arrow-left pull-right">Back</a>
                            <h3 class="panel-title">Patient payments</h3>
                        </div>

                        <div class="panel-body">
                            @if($patient->payments->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>PayId</th>
                                        <th>Payment type</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1
                                    @endphp
                                    @foreach($diagnosis_payments as $payment)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{$payment->category}}
                                            <td>{{ $payment->amount }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @else
                                    <div class="alert alert-info">
                                        This patient has not make any payments yet.
                                    </div>
                                    @endif
                                </div>
                                <div class="panel-footer">
                 
                                    </div>
                                             <a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/payments" class="fa fa-arrow-left pull-left">Back</a>
                                </div>

                            </div>
                            <div class="col-sm-2">
                                @include('partials/patient/sidebar')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection








































































